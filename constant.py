URL_INITIALIZATION = 'https://fw2.bry.com.br/api/xml-signature-service/v1/signatures/initialize'
URL_FINALIZATION = 'https://fw2.bry.com.br/api/xml-signature-service/v1/signatures/finalize'

AUTH_TOKEN = 'insert-a-valid-token'

PROFILE = 'BASIC'

BINARY_CONTENT = 'false'

OPERATION_TYPE = 'SIGNATURE'

SIGNATURE_FORMAT = 'ENVELOPED'

HASH_ALGORITHM = 'SHA256'

FILE_PATH = "./files-to-sign/pom.xml"
CERT_PATH = './caminho/para/o/certificado.p12'
CERT_PASSWORD = 'senha do certificado'