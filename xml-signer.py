import requests
import json
from OpenSSL import crypto
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA512, SHA256, SHA
from base64 import b64encode, b64decode
from constant import AUTH_TOKEN, PROFILE, BINARY_CONTENT, OPERATION_TYPE, SIGNATURE_FORMAT, HASH_ALGORITHM, FILE_PATH, CERT_PATH, CERT_PASSWORD, URL_FINALIZATION, URL_INITIALIZATION

file = open(FILE_PATH, 'rb').read()
HEADER = {'Authorization': 'Bearer ' + AUTH_TOKEN}

def token_verify():
    aux_authorization = HEADER['Authorization'].split(' ')
    if(aux_authorization[1] == 'insert-a-valid-token'):
        print("Insert a valid token")
        return False
    else:
        return True

def initialize_singature():

    print("================ Initializing XML Signature ... ================")
    print("")

# Step 1: Load private key and digital certificate content.

    original_documents = []
    original_documents.append(('originalDocuments[0][content]', file))

    p12 = crypto.load_pkcs12(open(CERT_PATH, 'rb').read(), CERT_PASSWORD)
    cert = p12.get_certificate()
   
    cert_base_64 = crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode('utf-8').replace('-----BEGIN CERTIFICATE-----', '').replace('-----END CERTIFICATE-----', '').replace('\n', '')


    initialization_form = {

        'nonce': 1, 
        'profile': PROFILE,
        'binaryContent': BINARY_CONTENT,
        'operationType': OPERATION_TYPE,
        'originalDocuments[0][nonce]': '1',
        'signatureFormat': SIGNATURE_FORMAT,
        'hashAlgorithm': HASH_ALGORITHM,
        'certificate': cert_base_64
    }

# Step 2: Initialization of the signature and production of the signedAttributes artifacts.

    response = requests.post(URL_INITIALIZATION, data = initialization_form, headers=HEADER, files=original_documents)
    if response.status_code == 200:

        data = response.json()
        print("Inicialization JSON response: ", data)
        print("")

        signed_attributes = data['signedAttributes']

        signed_attributes_content_decoded = b64decode(data['signedAttributes'][0]['content'])

        encrypted_message_digest_b64 = encrypt_initialized_signature(signed_attributes_content_decoded, p12.get_privatekey())

        finalize_signature(cert_base_64, data, encrypted_message_digest_b64)


    else:
        print(response.text)


def encrypt_initialized_signature(signed_attributes_content_decoded, private_key):

# Step 3: Encryption of data initialized with the certificate stored on disk.

    key = crypto.dump_privatekey(crypto.FILETYPE_PEM, private_key).decode("utf-8")
    rsa_key = RSA.importKey(key)

    if HASH_ALGORITHM == 'SHA1':
        hash_object = SHA.new(signed_attributes_content_decoded)
    elif HASH_ALGORITHM == 'SHA256':
        hash_object = SHA256.new(signed_attributes_content_decoded)
    elif HASH_ALGORITHM == 'SHA512':
        hash_object = SHA512.new(signed_attributes_content_decoded)
    else:
        print("Invalid signature hash algorithm, allowed: 'SHA1, SHA256, SHA512' ")
        
    signature = PKCS1_v1_5.new(rsa_key).sign(hash_object)

    return b64encode(signature)
        

def finalize_signature(cert_base_64, initialization_data, message_digest_cifrado_b64):

# Step 4: Finalization of the signature and obtaining the signed artifact.

    print("================ Finishing XML Signature ... ================")
    print("")

    signed_attributes = initialization_data['signedAttributes']

    form_finalization = {
        'nonce': 1, 
        'profile': PROFILE,
        'hashAlgorithm': HASH_ALGORITHM,
        'binaryContent': BINARY_CONTENT,
        'signatureFormat': SIGNATURE_FORMAT,
        'operationType': OPERATION_TYPE,
        'certificate': cert_base_64
        }

    original_documents = []
    original_documents.append(('finalizations[0][document]', file))

    for i in range(len(signed_attributes)):

        form_finalization['finalizations[0][nonce]'] = 1
        form_finalization['finalizations[0][initializedDocument]'] = initialization_data['initializedDocuments'][0]['content']
        form_finalization['finalizations[0][signatureValue]'] = message_digest_cifrado_b64

    response = requests.post(URL_FINALIZATION, data = form_finalization, headers=HEADER, files=original_documents)

    if response.status_code == 200:

        data = response.json()
        print("Finalization JSON response: ", data)
        print("")
        signature_content = data['signatures'][0]['content']
        signature_byte_array = b64decode(signature_content.encode("utf-8"))
        new_file = open("AssinaturaXML.xml", 'wb')
        new_file.write(signature_byte_array)
        new_file.close()

        print("Signature successful and stored in the local folder as 'AssinaturaXML.xml'")

    else:
        print(response.text)


if(token_verify()):
    initialize_singature()
